import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

let wrapper;

const setupWrapper = () => {
  return wrapper = shallow(<App />)
};

beforeEach(() => {
  setupWrapper();
});

describe('App.js', () => {
  it('App should not be undefined', () => {
    expect(wrapper).not.toBe(undefined);
  });
});
